<?php
/**
 * This view is used by console/controllers/MigrateController.php.
 *
 * The following variables are available in this view:
 */
/* @var $className string the new migration class name without namespace */
/* @var $namespace string the new migration class namespace */
/* @var $table string the name table */
/* @var $tableComment string the comment table */
/* @var $fields array the fields */
/* @var $foreignKeys array the foreign keys */

echo "<?php\n";
if (!empty($namespace)) {
    echo "\nnamespace {$namespace};\n";
}
?>

use yii\db\Migration;

/**
* Handles the creation of table `<?= $table ?>`.
<?= $this->render('_foreignTables', [
    'foreignKeys' => $foreignKeys,
]) ?>
*/
class <?= $className ?> extends \app\components\base\BaseMigration
{
public $table = '<?= $table ?>';

/**
* @return bool|void
* @throws \yii\base\Exception
*/
public function up()
{
$this->newTable([
'id' => $this->primaryKey(),
]);
}

/**
* {@inheritdoc}
*/
public function down()
{
$this->removeTable();
}
}
