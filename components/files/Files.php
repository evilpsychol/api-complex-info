<?php

namespace app\components\files;


use app\helpers\BehaviorsHelper;
use app\modules\complex\models\QueuesImages;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\ServerErrorHttpException;
use yii\web\UploadedFile;

/**
 * This is the model class for table "files".
 *
 * @property int $id
 * @property string $file_name
 * @property string|null $name
 * @property string $path
 * @property string $content_type
 * @property int $created
 *
 * @property QueuesImages[] $queuesImages
 */
class Files extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file_name', 'path', 'content_type'], 'required'],
            [['file_name', 'name', 'path'], 'string', 'max' => 50],
            [['content_type'], 'string', 'max' => 20],
        ];
    }

    public function behaviors()
    {
        return [BehaviorsHelper::time('created', FALSE)];
    }


    /**
     * @param UploadedFile $file
     * @param string $path
     * @return Files
     * @throws ServerErrorHttpException
     * @throws \yii\base\Exception
     */
    public static function saveFile(UploadedFile $file, string $path)
    {
        $model = new self([
            'file_name' => Yii::$app->security->generateRandomString(32) . '.' . $file->extension,
            'path' => $path,
            'content_type' => $file->type,
        ]);

        $path = Yii::getAlias("@uploads/$path");
        FileHelper::createDirectory($path);

        if (!$file->saveAs(Yii::getAlias($path . DIRECTORY_SEPARATOR . $model->file_name))) {
            throw new ServerErrorHttpException('Ошибка при сохранении файла');
        }
        $model->save(FALSE);

        return $model;
    }

    public function fields()
    {
        return [
            'id',
            'file_name',
            'name',
            'content_type',
            'path' => function () {
                return Url::to("api/uploads/{$this->path}/{$this->file_name}", true);
            },
            'created',
        ];
    }

    public function afterDelete()
    {
        parent::afterDelete();
        unlink(Yii::getAlias("@uploads/{$this->path}/{$this->file_name}"));
    }

}
