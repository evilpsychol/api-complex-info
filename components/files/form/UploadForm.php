<?php

namespace app\components\files\form;

use app\components\files\Files;
use yii\base\Model;


class UploadForm extends Model
{
    public $image;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image'], 'required'],
            [['image'], 'file', 'skipOnEmpty' => FALSE, 'extensions' => 'pdf,jpg,jpeg,png'],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'image' => 'Изображение',
        ];
    }


    public function upload()
    {
        if (!$this->validate()) {
            return false;
        }
        return Files::saveFile($this->image, 'queues');
    }

}
