<?php

namespace app\components\files\form;

use app\components\files\Files;
use yii\base\Model;
use yii\web\ServerErrorHttpException;


class UploadFilesForm extends Model
{
    public $images;
    public $path;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['images'], 'required'],
            [['images'], 'image',
                'extensions' => ['jpg', 'jpeg', 'png'],
                'checkExtensionByMimeType' => true,
                'maxFiles' => 10
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'images' => 'Изображения',
        ];
    }


    public function upload()
    {
        if (!$this->validate()) {
            return false;
        }
        if (!$this->path) {
            throw new ServerErrorHttpException('Не указан путь');
        }
        $res = [];
        foreach ($this->images as $image) {
            $res[] = Files::saveFile($image, $this->path);
        }
        return $res;
    }

}
