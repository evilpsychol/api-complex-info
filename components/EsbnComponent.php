<?php

namespace app\components;


use app\modules\esbn\models\EsbnDistricts;
use app\modules\esbn\models\EsbnDistrictsImages;
use Yii;
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii2mod\ftp\FtpClient;

class EsbnComponent extends BaseObject
{
    const DOMEN = 'http://esbnbase.beget.tech/';
    const FTP_DIR_XML = 'esbn/flats/feeds/esbn/';
    const FTP_DIR_XML_COMMERCIAL = 'esbn/commercial/feeds/esbn/';

    private $_ftp = [];

    public function init()
    {
        parent::init();
        $this->_ftp = [
            'host' => env('ESBN_HOST'),
            'login' => env('ESBN_LOGIN'),
            'password' => env('ESBN_PASSWORD'),
        ];
    }

    /**
     * Парсинг XML
     */
    public function parsingXml()
    {
        foreach ([self::FTP_DIR_XML, self::FTP_DIR_XML_COMMERCIAL] as $path) {
            $ftp = $this->connectDir($path);
            $nameFile = current(array_values($ftp->scanDir()))['name'];
            if ($ftp->get($nameFile, $nameFile, FTP_BINARY)) {
                $xml = simplexml_load_file($nameFile);
                foreach (array_keys(ArrayHelper::toArray($xml->children())) as $child) {
                    $this->insertData($xml->$child, $path == self::FTP_DIR_XML ? 'flats/' : 'commercial/');
                }
            } else {
                Yii::error('Файл XML не удалось загрузить!', __METHOD__);
            }
        }
    }


    public function insertData(\SimpleXMLElement $childs, $pathImg)
    {
        $name = 'esbn_' . self::fromCamel($childs->getName());
        $data = $this->formatData($childs->children(), $pathImg);

        $db = Yii::$app->db;
        if (!$db->createCommand("SHOW TABLES  like '{$name}'")->execute()) {
            return;
        }

        $className = "\\app\\modules\\esbn\\models\\" . Inflector::camelize($name);
        $tableColumns = $className::getTableSchema()->columnNames;
        $forColumns = [];
        $insertRows = 0;
        foreach ($data as $item) {
            $missingColumns = array_values(array_diff(array_keys($item), $tableColumns));
            if ($missingColumns) {
                $forColumns += $missingColumns;
                $item = array_diff_key($item, array_flip($missingColumns));
            }
            $insertRows += $db->createCommand()->upsert($name, $item)->execute();
        }
        if ($name === EsbnDistricts::tableName()) {
            foreach ($childs->children() as $child) {
                $db->createCommand()->upsert(EsbnDistrictsImages::tableName(), [
                    'esbn_district_id' => (int)$child->attributes()->id,
                    'image' => "$pathImg" . (string)$child->pictures->children()->picture->attributes()->bigUrl
                ])->execute();
            }

        }

        echo "$name:  $insertRows\n";
        if ($forColumns) {
            echo "Нет колонок:  " . implode(', ', array_unique($forColumns)) . "\n";
        }
    }

    public static function fromCamel($str)
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $str));
    }

    public function formatData(\SimpleXMLElement $childs, $pathImg)
    {
        $data = [];
        foreach ($childs as $i => $child) {
            $item = [];
            foreach ($child->attributes() as $key => $val) {
                $key = str_replace('-', '_', $key);
                $key = self::fromCamel($key);
                if ($key == 'living_square') {
                    $val = str_replace(',', '.', $val);
                }
                if ($key == 'plane_pic_url' && $val) {
                    $val = $pathImg . $val;
                }
                $item[$key] = (string)$val;
            }
            $data[] = $item;
        }
        return $data;
    }

    /**
     * Чтение директории ftp
     * @param $dir
     * @return FtpClient
     * @throws \yii2mod\ftp\FtpException
     */
    public function connectDir($dir)
    {
        $ftp = new FtpClient();
        $ftp->connect($this->_ftp['host']);
        $ftp->login($this->_ftp['login'], $this->_ftp['password']);
        $ftp->pasv(TRUE);
        $ftp->chdir($dir);

        return $ftp;
    }
}
