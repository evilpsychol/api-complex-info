<?php

namespace app\components\base;

use yii\filters\AccessControl;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;

class BaseApiController extends Controller
{
    public $enableCsrfValidation = FALSE;
    protected $rules = [
        [
            'allow' => TRUE,
            'roles' => ['@'],
        ],
    ];
    protected $only = [];
    protected $optionalAuth = [];
    protected $requiredAuth = ['*'];
    protected $actions = [];

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $auth = $behaviors['authenticator'];
        unset($behaviors['authenticator']);
        $actionsAuth = array_merge($this->requiredAuth, $this->optionalAuth);
        if (!empty($actionsAuth)) {
            $auth = [
                'class' => CompositeAuth::class,
                'only' => $actionsAuth,
                'optional' => $this->optionalAuth,
                'authMethods' => [
                    HttpBearerAuth::class,
                ],
            ];
        }

        $behaviors['corsFilter'] = [
            'class' => Cors::class,
            'cors' => [
                'Origin' => [
                    'http://localhost:3000',
                    'http://complex-info',
                ],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Expose-Headers' => [
                    'X-Pagination-Total-Count',
                    'X-Pagination-Current-Page',
                    'X-Pagination-Page-Count',
                    'X-Pagination-Per-Page',
                ],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Max-Age' => 86400,
            ],
        ];

        $behaviors['authenticator'] = $auth;
        $behaviors['authenticator']['except'] = ['options'];
        if (count($this->rules)) {
            $behaviors['access'] = [
                'class' => AccessControl::class,
                'only' => $this->only,
                'rules' => $this->rules,
            ];
        }

        return $behaviors;
    }

    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), $this->methods());
    }

    public function methods()
    {
        return [];
    }

}
