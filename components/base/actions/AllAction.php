<?php

namespace app\components\base\actions;

use app\components\base\BaseApiAction;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;


class AllAction extends BaseApiAction
{
    public $orderBy;

    public function run()
    {
        /** @var ActiveQuery $query */
        $query = $this->modelClass::find();
        if ($this->orderBy) {
            $query->orderBy($this->orderBy);
        }
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => FALSE,
        ]);
    }
}
