<?php

namespace app\components\base\actions;

use app\components\base\BaseApiAction;
use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\web\ServerErrorHttpException;


class CreateOrUpdateAction extends BaseApiAction
{
    public $scenario = Model::SCENARIO_DEFAULT;

    public $paramFind;

    /**
     * @param $id
     * @return ActiveRecord
     * @throws ServerErrorHttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id)
    {
        /* @var $model ActiveRecord */
        $modelClass = $this->modelClass;
        $params = [$this->paramFind => $id];
        /* @var $modelClass ActiveRecord */
        $model = $modelClass::findOne($params);
        if(!$model){
            $model = new $modelClass($params);
        }

        $model->scenario = $this->scenario;
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        if ($model->save() === false && !$model->hasErrors()) {
            throw new ServerErrorHttpException('Неизвестная ошибка при сохранении');
        }

        return $model;
    }
}
