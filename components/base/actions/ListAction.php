<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\components\base\actions;

use app\components\base\BaseApiAction;
use Yii;
use yii\base\Model;


class ListAction extends BaseApiAction
{
    public $searchClass;
    public $scenario = Model::SCENARIO_DEFAULT;


    public function run()
    {
        /** @var Model $searchModel */
        $searchModel = new $this->searchClass();
        $searchModel->setScenario($this->scenario);
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $dataProvider;
    }
}
