<?php

namespace app\components\base;

use yii\db\Migration;

class BaseMigration extends Migration
{
    public $table;
    public $tablesRefs;

    public function upFk()
    {
        foreach ($this->tablesRefs as $ref_id => $ref) {
            $this->addForeignKey($this->nameFK($ref_id),
                $this->table, $ref['column'],
                $ref['table'], $ref['refColumn'],
                $ref['delete'], $ref['update']);
        }
    }

    public function nameFK($index)
    {
        $table = str_replace('.', '', $this->table);
        $table_refs = $this->tablesRefs[$index];
        $tableRefsName = str_replace('.', '', $table_refs['table']);

        return "fk_{$table}__{$tableRefsName}__{$table_refs['column']}__{$table_refs['refColumn']}";
    }

    public function downFk()
    {
        foreach (array_keys($this->tablesRefs) as $ref_id) {
            $this->dropForeignKey($this->nameFK($ref_id), $this->table);
        }

    }

    public function createIndexKey($columns, $unique = TRUE)
    {
        parent::createIndex($this->indexKey($columns, $unique), $this->table, $columns, $unique);
    }

    public function indexKey($columns, $unique)
    {
        $strColumns = implode('_', $columns);
        $table = str_replace('.', '', $this->table);
        $uniqStr = $unique ? '__unique' : '';

        return "{$table}{$uniqStr}__{$strColumns}";
    }

    public function dropIndexKey($columns, $unique = TRUE)
    {
        parent::dropIndex($this->indexKey($columns, $unique), $this->table);
    }

    public function newTable($columns, $options = NULL)
    {
        parent::createTable($this->table, $columns, $options);
    }

    public function removeTable()
    {
        parent::dropTable($this->table);
    }

    public function point()
    {
        return $this->getDb()->getSchema()->createColumnSchemaBuilder('POINT');
    }

    public function addColumns($columns)
    {
        foreach ($columns as $name => $type) {
            $this->addColumn($this->table, $name, $type);
        }
    }

    public function dropColumns($columns)
    {
        foreach ($columns as $name => $type) {
            $this->dropColumn($this->table, $name);
        }
    }
}
