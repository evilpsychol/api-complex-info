<?php

namespace app\rbac;

use app\modules\user\models\Users;
use Yii;
use yii\rbac\Rule;

class UserRule extends Rule
{
    public $name = 'user';

    public function execute($user, $item, $params)
    {
        if (!Yii::$app->user->isGuest) {
            /** @var Users $userIdentity */
            $userIdentity = Yii::$app->user->identity;

            $rules = array_flip(Users::ROLES);

            $childRoles = array_keys(Yii::$app->authManager->getChildRoles($rules[$userIdentity->role]));

            return in_array($item->name, $childRoles);
        }

        return FALSE;
    }
}
