<?php
return [
    'admin' => [
        'type' => 1,
        'ruleName' => 'user',
        'children' => [
            'user',
        ],
    ],
    'user' => [
        'type' => 1,
        'ruleName' => 'user',
    ],
];
