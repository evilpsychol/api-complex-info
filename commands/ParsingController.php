<?php

namespace app\commands;

use app\components\EsbnComponent;
use yii\console\Controller;

class ParsingController extends Controller
{

    public function actionEsbn()
    {
        (new EsbnComponent())->parsingXml();
    }

}
