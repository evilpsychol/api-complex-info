<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\modules\user\models\Users;
use app\rbac\UserRule;
use Yii;
use yii\console\Controller;
use yii\rbac\ManagerInterface;

class RbacController extends Controller
{
    /** @var ManagerInterface */
    private $_auth;
    private $_rule;

    public function init()
    {
        $this->_auth = Yii::$app->authManager;
        $this->_rule = new UserRule();
        parent::init();
    }

    /**
     * @throws \yii\base\Exception
     * @throws \Exception
     */
    public function actionInit()
    {
        $this->_auth->removeAll();

        $this->_auth->add($this->_rule);

        $this->createRole(Users::ROLE_ADMIN);
        $this->createRole(Users::ROLE_USER);

        $this->addChild(Users::ROLE_ADMIN, Users::ROLE_USER);
    }

    private function createRole($name)
    {
        $model = $this->_auth->createRole($name);
        $model->ruleName = $this->_rule->name;
        $this->_auth->add($model);
    }

    private function addChild($parent, $child)
    {
        $this->_auth->addChild($this->_auth->getRole($parent), $this->_auth->getRole($child));
    }

    private function createPermission($name)
    {
        $model = $this->_auth->createPermission($name);
        $model->ruleName = $this->_rule->name;
        $this->_auth->add($model);
    }
}
