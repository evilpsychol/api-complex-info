<?php

namespace app\commands;

use app\modules\user\models\Users;
use yii\console\Controller;

class UserController extends Controller
{


    public function actionCreate()
    {
        foreach ($this->users() as $user) {
            if (Users::findOne(['login' => $user['login']])) {
                echo "Пользователь {$user['login']} существует\n";
            }
            $model = new Users([
                'login' => $user['login'],
                'role' => $user['role'],
                'password_hash' => \Yii::$app->security->generatePasswordHash($user['password']),
            ]);
            if ($model->save()) {
                echo "Пользователь {$user['login']} добавлен\n";
            } else {
                var_dump($model->errors);
            }
        }
    }

    private function users()
    {
        return [
            [
                'login' => 'admin',
                'password' => env('ADMIN_PASS'),
                'role' => Users::ROLES[Users::ROLE_ADMIN]
            ],
            [
                'login' => 'user',
                'password' => env('USER_PASS'),
                'role' => Users::ROLES[Users::ROLE_USER]
            ],
        ];
    }

}
