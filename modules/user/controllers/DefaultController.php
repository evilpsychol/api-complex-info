<?php

namespace app\modules\user\controllers;


use app\components\base\BaseApiController;
use app\modules\user\models\form\LoginForm;
use app\modules\user\models\Users;
use app\modules\user\models\UsersTokens;
use Yii;
use yii\filters\auth\HttpBearerAuth;

/**
 * Default controller for the `users` module
 */
class DefaultController extends BaseApiController
{
    protected $requiredAuth = ['self', 'logout'];
    protected $rules = [
        [
            'allow' => TRUE,
            'actions' => ['self', 'logout'],
            'roles' => ['@'],
        ],
        [
            'allow' => TRUE,
            'actions' => ['login'],
            'roles' => ['?'],
        ],
    ];


    public function actionLogin()
    {
        $form = new LoginForm();
        $form->load(Yii::$app->request->post(), '');
        if (!$form->validate()) {
            return $form;
        }

        $user = Users::findOne(['login' => $form->login]);
        if (!empty($user) && Yii::$app->security->validatePassword($form->password, $user->password_hash)) {
            return UsersTokens::createToken($user->id);
        }
        $form->addError('login', 'Неверная комбинация логина и пароля');
        $form->addError('password', 'Неверная комбинация логина и пароля');

        return $form;
    }

    public function actionSelf()
    {
        return Yii::$app->user->identity;
    }

    public function actionLogout()
    {
        /** @var HttpBearerAuth $auth */
        $auth = $this->getBehavior('authenticator')->authMethods[0];
        preg_match($auth->pattern, Yii::$app->request->headers->get($auth->header), $matches);
        $model = UsersTokens::findOne(['user_id' => Yii::$app->user->id, 'token' => $matches[1]]);

        return $model ? $model->delete() : null;
    }
}
