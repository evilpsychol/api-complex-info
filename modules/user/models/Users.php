<?php

namespace app\modules\user\models;


use app\helpers\BehaviorsHelper;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $login
 * @property string $password_hash
 * @property int $role
 * @property bool $active
 * @property int $created
 *
 */
class Users extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const ROLE_USER = 'user';
    const ROLE_ADMIN = 'admin';
    const ROLES = [
        self::ROLE_USER => 1,
        self::ROLE_ADMIN => 2,
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = NULL)
    {
        return self::find()->joinWith(['tokens'])
            ->andWhere([
                'users_tokens.token' => $token,
            ])
            ->andWhere(['>=', 'expires', time()])->one();
    }


    public function behaviors()
    {
        return [BehaviorsHelper::time('created', FALSE)];
    }

    public function fields()
    {
        return [
            'id',
            'login',
            'role' => function () {
                return array_flip(self::ROLES)[$this->role];
            },
            'active',
            'created',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTokens()
    {
        return $this->hasMany(UsersTokens::class, ['user_id' => 'id']);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return TRUE;
    }

    public function validateAuthKey($authKey)
    {
        return TRUE;
    }
}
