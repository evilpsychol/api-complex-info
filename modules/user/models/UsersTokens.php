<?php

namespace app\modules\user\models;

use app\helpers\BehaviorsHelper;
use Yii;
use yii\behaviors\AttributesBehavior;
use yii\db\ActiveRecord;
use yii\web\ServerErrorHttpException;

/**
 * This is the model class for table "main.users_tokens".
 *
 * @property string $token
 * @property int $user_id
 * @property string $ip
 * @property int $created
 * @property int $expires
 */
class UsersTokens extends \yii\db\ActiveRecord
{
    const DEFAULT_EXPIRES = 60 * 60 * 24 * 30;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users_tokens';
    }

    /**
     * Создание токена
     * @param $userId
     * @param float|int $expires
     * @return UsersTokens
     * @throws ServerErrorHttpException
     */
    public static function createToken($userId, $expires = self::DEFAULT_EXPIRES)
    {
        $model = new self();
        $model->setAttributes([
            'user_id' => $userId,
            'expires' => time() + $expires,
        ]);

        if (!$model->save() && !$model->hasErrors()) {
            throw new ServerErrorHttpException('Ошибка при создании токена');
        }

        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['token', 'user_id', 'ip', 'expires'], 'required'],
            [['created', 'expires'], 'integer'],
            [['token', 'ip'], 'string', 'max' => 50],
            [['token'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => TRUE, 'targetClass' => Users::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [
            BehaviorsHelper::time('created', FALSE),
            [
                'class' => AttributesBehavior::class,
                'attributes' => [
                    'token' => [
                        ActiveRecord::EVENT_BEFORE_VALIDATE => Yii::$app->security->generateRandomString(),
                    ],
                    'ip' => [
                        ActiveRecord::EVENT_BEFORE_VALIDATE => Yii::$app->request->getUserIP(),
                    ],
                ],
            ],
        ];
    }
}
