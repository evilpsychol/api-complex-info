<?php

namespace app\modules\user\models\form;

use yii\base\Model;

class LoginForm extends Model
{
    public $login;
    public $password;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['password', 'login'], 'required'],
            [['password'], 'string', 'min' => 8],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
            'login' => 'Логин',
        ];
    }

}
