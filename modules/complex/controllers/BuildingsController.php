<?php

namespace app\modules\complex\controllers;

use app\components\base\actions\CreateOrUpdateAction;
use app\components\base\BaseApiController;
use app\modules\complex\controllers\buildings\PositionAction;
use app\modules\complex\models\Buildings;
use app\modules\user\models\Users;


class BuildingsController extends BaseApiController
{
    protected $rules = [
        [
            'allow' => TRUE,
            'actions' => ['change', 'position'],
            'roles' => [Users::ROLE_ADMIN],
        ],
    ];

    public function methods()
    {
        return [
            'change' => [
                'class' => CreateOrUpdateAction::class,
                'modelClass' => Buildings::class,
                'paramFind' => 'esbn_id'
            ],
            'position' => PositionAction::class
        ];
    }

}
