<?php

namespace app\modules\complex\controllers;

use app\components\base\actions\ListAction;
use app\components\base\BaseApiController;
use app\modules\complex\controllers\flats\FilterAction;
use app\modules\complex\models\search\ApartmentsSearch;
use app\modules\user\models\Users;


class FlatsController extends BaseApiController
{
    protected $rules = [
        [
            'allow' => TRUE,
            'actions' => ['list', 'filter'],
            'roles' => [Users::ROLE_USER],
        ],
    ];

    public function methods()
    {
        return [
            'list' => [
                'class' => ListAction::class,
                'searchClass' => ApartmentsSearch::class
            ],
            'filter' => FilterAction::class
        ];
    }

}
