<?php

namespace app\modules\complex\controllers;

use app\components\base\BaseApiController;
use app\modules\complex\controllers\queues\ChangeBuildingAction;
use app\modules\complex\controllers\queues\DeleteImageAction;
use app\modules\complex\controllers\queues\ImageAction;
use app\modules\complex\controllers\queues\ListAction;
use app\modules\complex\models\Queues;
use app\modules\user\models\Users;
use yii\rest\CreateAction;
use yii\rest\DeleteAction;
use yii\rest\UpdateAction;
use yii\web\UnprocessableEntityHttpException;


class QueuesController extends BaseApiController
{
    protected $rules = [
        [
            'allow' => TRUE,
            'actions' => ['add', 'change-building', 'update', 'delete', 'image', 'delete-image'],
            'roles' => [Users::ROLE_ADMIN],
        ],
        [
            'allow' => TRUE,
            'actions' => ['list'],
            'roles' => [Users::ROLE_USER],
        ],
    ];

    public function methods()
    {
        return [
            'add' => [
                'class' => CreateAction::class,
                'modelClass' => Queues::class
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => Queues::class,
                'checkAccess' => function ($action, $model) {
                    /** @var Queues $model */
                    $count = (int)$model->getBuildings()->count();
                    if ($count) {
                        throw new UnprocessableEntityHttpException('Нельзя удалить очередь с корпусами');
                    }
                }
            ],
            'update' => [
                'class' => UpdateAction::class,
                'modelClass' => Queues::class,
                'scenario' => Queues::SCENARIO_UPDATE
            ],
            'list' => ListAction::class,
            'image' => ImageAction::class,
            'delete-image' => DeleteImageAction::class,
            'change-building' => ChangeBuildingAction::class,
        ];
    }

}
