<?php

namespace app\modules\complex\controllers\progress;


use app\components\base\BaseApiAction;
use app\components\files\form\UploadFilesForm;
use app\helpers\ModelHelper;
use app\modules\complex\models\Progress;
use app\modules\complex\models\ProgressImages;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

class ImageAction extends BaseApiAction
{
    public function run($id)
    {
        /** @var Progress $progress */
        $progress = ModelHelper::find(Progress::class, $id);
        $form = new UploadFilesForm([
            'path' => 'progress',
            'images' => UploadedFile::getInstancesByName('images'),
        ]);

        if (!$files = $form->upload()) {
            return $form;
        }

        ProgressImages::addImages($progress->id, ArrayHelper::getColumn($files, 'id'));

        return $files;
    }
}
