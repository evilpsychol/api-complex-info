<?php

namespace app\modules\complex\controllers\progress;


use app\components\base\BaseApiAction;
use app\modules\complex\models\Progress;
use Yii;

class ChangeAction extends BaseApiAction
{
    public function run()
    {
        $post = Yii::$app->request->post();
        $model = new Progress();
        $model->load($post, '');
        if (!$model->validate()) {
            return $model;
        }
        list($year, $month) = explode('-', $model->date);
        $find = Progress::findOne([
            'year' => $year,
            'month' => (int)$month,
            'complex_id' => $model->complex_id,
        ]);
        if ($find) {
            $find->load($post, '');
            $model = $find;
        }
        if (!$model->save()) {
            return $model;
        }


        return $model;
    }
}
