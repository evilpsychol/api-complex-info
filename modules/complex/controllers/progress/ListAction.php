<?php

namespace app\modules\complex\controllers\progress;


use app\components\base\BaseApiAction;
use app\helpers\ModelHelper;
use app\modules\complex\models\Complexes;
use app\modules\complex\models\Progress;

class ListAction extends BaseApiAction
{
    public function run($id)
    {
        /** @var Complexes $complex */
        $complex = ModelHelper::find(Complexes::class, $id);


        return Progress::findAll(['complex_id' => $complex]);
    }
}
