<?php

namespace app\modules\complex\controllers\flats;


use app\components\base\BaseApiAction;
use app\modules\complex\models\Complexes;
use app\modules\complex\models\Queues;
use app\modules\esbn\models\EsbnApartments;
use app\modules\esbn\models\EsbnBuildings;
use app\modules\esbn\models\EsbnCardinalDirections;
use app\modules\esbn\models\EsbnFacings;
use app\modules\esbn\models\EsbnRoomNumbers;
use Yii;
use yii\base\DynamicModel;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class FilterAction extends BaseApiAction
{
    public function run($id)
    {
        $complex = Complexes::findOne($id);
        if (!$complex) {
            throw new NotFoundHttpException('Комплекс не найден');
        }

        $params = (new DynamicModel(['queue_id']))
            ->addRule(['queue_id'], 'integer');
        $params->load(Yii::$app->request->get(), '');
        if (!$params->validate()) {
            return $params;
        }


        $queues = $this->queues($complex->id);
        return [
            'queues' => $queues,
            'buildings' => $this->buildings(ArrayHelper::getColumn($queues, 'value'), $params->queue_id),
            'ending' => $this->ending($complex->esbn_id),
            'rooms' => $this->rooms($complex->esbn_id),
            'facing' => $this->facing($complex->esbn_id),
            'windows' => $this->windows($complex->esbn_id),
            'range' => $this->range($complex->esbn_id),
        ];
    }

    private function queues($complexId)
    {
        return Queues::find()
            ->select(['value' => 'id', 'text' => 'name'])
            ->andWhere(['complex_id' => $complexId])
            ->asArray()
            ->all();
    }

    private function buildings($queueIds, $queueId)
    {
        $query = EsbnBuildings::find()->joinWith(['queue']);
        if (count($queueIds) > 1 && $queueId) {
            $query->andFilterWhere(['queues.id' => $queueId]);
        } elseif (count($queueIds) == 1) {
            $query->andFilterWhere(['queues.id' => $queueIds]);
        } else {
            return null;
        }

        return array_map(function ($e) {
            return [
                'value' => $e->id,
                'text' => $e->name
            ];
        }, $query->all());
    }

    private function ending($district)
    {
        $query = EsbnBuildings::find()
            ->select(['ending_quarter', 'ending_year'])
            ->andWhere(['district' => $district])
            ->andWhere(['is not', 'ending_year', null])
            ->andWhere(['is not', 'ending_quarter', null])
            ->orderBy(['ending_year' => SORT_ASC, 'ending_quarter' => SORT_ASC])
            ->groupBy(['ending_quarter', 'ending_year']);
        return array_map(function ($e) {
            return [
                'value' => $e->ending_year . '-' . $e->ending_quarter,
                'text' => $e->ending
            ];
        }, $query->all());
    }

    private function rooms($district)
    {
        $query = EsbnRoomNumbers::find()
            ->select(['value' => 'esbn_room_numbers.id', 'text' => 'esbn_room_numbers.title'])
            ->joinWith(['apartments.buildingInfo'])
            ->andWhere(['esbn_buildings.district' => $district])
            ->orderBy(['esbn_room_numbers.id' => SORT_ASC])
            ->groupBy(['esbn_room_numbers.id'])->asArray();

        return array_map(function ($e) {
            return [
                'value' => $e['value'],
                'text' => $e['text'],
            ];
        }, @$query->all());

    }

    private function facing($district)
    {
        $query = EsbnFacings::find()
            ->select(['value' => 'esbn_facings.id', 'text' => 'esbn_facings.title'])
            ->joinWith(['apartments.buildingInfo'])
            ->andWhere(['esbn_buildings.district' => $district])
            ->orderBy(['esbn_facings.id' => SORT_ASC])
            ->groupBy(['esbn_facings.id'])->asArray();
        return array_map(function ($e) {
            return [
                'value' => $e['value'],
                'text' => $e['text'],
            ];
        }, @$query->all());
    }

    private function windows($district)
    {
        $query = EsbnCardinalDirections::find()
            ->select(['value' => 'esbn_cardinal_directions.id', 'text' => 'esbn_cardinal_directions.title'])
            ->joinWith(['apartments.buildingInfo'])
            ->andWhere(['esbn_buildings.district' => $district])
            ->orderBy(['esbn_cardinal_directions.id' => SORT_ASC])
            ->groupBy(['esbn_cardinal_directions.id'])->asArray();
        return array_map(function ($e) {
            return [
                'value' => $e['value'],
                'text' => EsbnCardinalDirections::NAMES[$e['text']],
            ];
        }, @$query->all());
    }

    private function range($district)
    {
        $params = [
            'price' => 'apartment_price',
            'floor' => 'floor',
            'square' => 'square',
            'kitchen' => 'kitchen_square',
        ];
        $selects = [];
        foreach ($params as $name => $column) {
            foreach (['min', 'max'] as $type) {
                $selects["{$type}_{$name}"] = "{$type}(esbn_apartments.{$column})";
            }
        }
        $query = EsbnApartments::find()
            ->select($selects)
            ->joinWith(['buildingInfo'])
            ->andWhere(['esbn_buildings.district' => $district])
            ->asArray();
        $v = @$query->one();

        $res = [];
        foreach ($params as $name => $column) {
            foreach (['min', 'max'] as $type) {
                $res[$name][$type] = $v["{$type}_{$name}"];
            }
        }

        return $res;
    }
}
