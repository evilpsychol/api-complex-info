<?php

namespace app\modules\complex\controllers\queues;


use app\components\base\BaseApiAction;
use app\modules\complex\models\Complexes;
use app\modules\complex\models\Queues;
use app\modules\complex\models\QueuesBuilding;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class ListAction extends BaseApiAction
{
    public function run($id)
    {
        $complex = Complexes::findOne($id);
        if (!$complex)
            throw new NotFoundHttpException('Комплекс не найден');


        $queues = Queues::findAll(['complex_id' => $complex->id]);
        // все корпуса комплекса
        $esbnBuildings = $complex->esbn->buildings;
        if (!$esbnBuildings) {
            return $queues;
        }

        // проверяем, во всех ли очередях все корпусы, если все, то возвращаем очереди
        $esbnBuildingsIds = ArrayHelper::getColumn($esbnBuildings, 'id');
        $buildings = QueuesBuilding::find()
            ->andWhere(['building_id' => $esbnBuildingsIds])
            ->all();
        $missingBuildingsIds = array_values(array_diff($esbnBuildingsIds, ArrayHelper::getColumn($buildings, 'building_id')));
        if (!empty($missingBuildingsIds)) {
            // если корпусов не хватает, а очередь одна, корпуса добавляем в нее
            if (count($queues) == 1) {
                QueuesBuilding::insertBuildings($queues[0]->id, $missingBuildingsIds);
            } else {
                $queue = new Queues(['complex_id' => $complex->id, 'name' => 'Нет очередей']);
                $queue->save(false);
                QueuesBuilding::insertBuildings($queue->id, $missingBuildingsIds);
            }
        }


        return Queues::findAll(['complex_id' => $complex->id]);
    }
}
