<?php

namespace app\modules\complex\controllers\queues;


use app\components\base\BaseApiAction;
use app\modules\complex\models\Queues;
use yii\web\NotFoundHttpException;

class DeleteImageAction extends BaseApiAction
{
    public function run($id)
    {

        $queue = Queues::findOne($id);
        if (!$queue) {
            throw new NotFoundHttpException('Очередь не найдена');
        }
        if (!$queue->image) {
            throw new NotFoundHttpException('Изображение отсутвует');
        }

        return $queue->image->delete();
    }
}
