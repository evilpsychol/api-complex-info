<?php

namespace app\modules\complex\controllers\queues;


use app\components\base\BaseApiAction;
use app\modules\complex\models\BuildingsPosition;
use app\modules\complex\models\Complexes;
use app\modules\complex\models\Queues;
use app\modules\complex\models\QueuesBuilding;
use app\modules\esbn\models\EsbnBuildings;
use Yii;
use yii\base\DynamicModel;
use yii\web\NotFoundHttpException;

class ChangeBuildingAction extends BaseApiAction
{
    public function run()
    {
        $params = (new DynamicModel(['building_id', 'queue_id']))
            ->addRule(['building_id', 'queue_id'], 'required')
            ->addRule(['building_id', 'queue_id'], 'integer')
            ->addRule(['building_id'], 'exist', ['skipOnError' => TRUE, 'targetClass' => EsbnBuildings::class, 'targetAttribute' => ['building_id' => 'id']])
            ->addRule(['queue_id'], 'exist', ['skipOnError' => TRUE, 'targetClass' => Queues::class, 'targetAttribute' => ['queue_id' => 'id']]);
        $params->load(Yii::$app->request->post(), '');
        $params->setAttributeLabel('queue_id', 'Очередь');
        if (!$params->validate()) {
            return $params;
        }

        $building = EsbnBuildings::findOne($params->building_id);
        $queue = Queues::findOne($params->queue_id);
        $complex = Complexes::findOne(['esbn_id' => $building->district]);
        if (!$complex) {
            throw new NotFoundHttpException('Комплекс не добавлен');
        }

        if ($queue->complex_id !== $complex->id) {
            throw new NotFoundHttpException('Очередь и корпус относятся к разным комплексам');
        }
        $db = Yii::$app->db;
        // удаляем положение на изображении, если корпус переносится
        $db->createCommand()->delete(BuildingsPosition::tableName(), ['building_id' => $params->building_id])->execute();
        return $db->createCommand()
            ->upsert(QueuesBuilding::tableName(), [
                'queue_id' => $params->queue_id,
                'building_id' => $params->building_id,
            ], ['queue_id' => $params->queue_id])->execute();
    }
}
