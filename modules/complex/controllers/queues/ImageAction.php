<?php

namespace app\modules\complex\controllers\queues;


use app\components\base\BaseApiAction;
use app\components\files\form\UploadForm;
use app\modules\complex\models\Queues;
use app\modules\complex\models\QueuesImages;
use yii\web\NotFoundHttpException;
use yii\web\UnprocessableEntityHttpException;
use yii\web\UploadedFile;

class ImageAction extends BaseApiAction
{
    public function run($id)
    {

        $queue = Queues::findOne($id);
        if (!$queue) {
            throw new NotFoundHttpException('Очередь не найдена');
        }
        if ($queue->image) {
            throw new UnprocessableEntityHttpException('Изображение для очереди уже загружено');
        }

        $form = new UploadForm([
            'image' => UploadedFile::getInstanceByName('image'),
        ]);

        if (!$file = $form->upload()) {
            return $form;
        }

        QueuesImages::addImage($queue->id, $file);

        return $file;
    }
}
