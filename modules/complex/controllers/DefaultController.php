<?php

namespace app\modules\complex\controllers;

use app\components\base\actions\AllAction;
use app\components\base\BaseApiController;
use app\modules\complex\models\Complexes;
use app\modules\user\models\Users;
use yii\rest\CreateAction;
use yii\rest\ViewAction;

/**
 * Default controller for the `complex` module
 */
class DefaultController extends BaseApiController
{
    protected $rules = [
        [
            'allow' => TRUE,
            'actions' => ['add'],
            'roles' => [Users::ROLE_ADMIN],
        ],
        [
            'allow' => TRUE,
            'actions' => ['list', 'one'],
            'roles' => [Users::ROLE_USER],
        ],
    ];

    public function methods()
    {
        return [
            'list' => [
                'class' => AllAction::class,
                'modelClass' => Complexes::class,
                'orderBy' => 'title'
            ],
            'add' => [
                'class' => CreateAction::class,
                'modelClass' => Complexes::class
            ],
            'one' => [
                'class' => ViewAction::class,
                'modelClass' => Complexes::class
            ]
        ];
    }

}
