<?php

namespace app\modules\complex\controllers;

use app\components\base\BaseApiController;
use app\components\files\Files;
use app\modules\complex\controllers\progress\ChangeAction;
use app\modules\complex\controllers\progress\ImageAction;
use app\modules\complex\controllers\progress\ListAction;
use app\modules\complex\models\Progress;
use app\modules\user\models\Users;
use yii\rest\DeleteAction;


class ProgressController extends BaseApiController
{
    protected $rules = [
        [
            'allow' => TRUE,
            'actions' => ['change', 'delete', 'image', 'delete-image'],
            'roles' => [Users::ROLE_ADMIN],
        ],
        [
            'allow' => TRUE,
            'actions' => ['list'],
            'roles' => [Users::ROLE_USER],
        ],
    ];

    public function methods()
    {
        return [
            'change' => ChangeAction::class,
            'list' => ListAction::class,
            'image' => ImageAction::class,
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => Progress::class
            ],
            'delete-image' => [
                'class' => DeleteAction::class,
                'modelClass' => Files::class
            ]
        ];
    }

}
