<?php

namespace app\modules\complex\controllers\buildings;


use app\components\base\BaseApiAction;
use app\modules\complex\models\BuildingsPosition;
use app\modules\esbn\models\EsbnBuildings;
use Yii;
use yii\web\NotFoundHttpException;

class PositionAction extends BaseApiAction
{
    public function run($id)
    {
        $building = EsbnBuildings::findOne($id);
        if (!$building) {
            throw new NotFoundHttpException('Корпус не найден');
        }
        $findParams = ['building_id' => $building->id,];
        $model = BuildingsPosition::findOne($findParams);
        if (!$model) {
            $model = new BuildingsPosition($findParams);
        }
        $model->load(Yii::$app->request->post(), '');
        $model->save();
        return $model;
    }
}
