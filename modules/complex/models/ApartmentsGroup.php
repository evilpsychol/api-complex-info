<?php

namespace app\modules\complex\models;

use app\modules\esbn\models\EsbnApartments;
use app\modules\esbn\models\EsbnBuildings;
use app\modules\esbn\models\EsbnRoomNumbers;

/**
 * This is the model class for table "apartments_group".
 *
 * @property int|null $building
 * @property int|null $room_number
 * @property float|null $square
 * @property int|null $min_price
 * @property int|null $max_price
 * @property int $count
 */
class ApartmentsGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'apartments_group';
    }

    public static function primaryKey()
    {
        return ['building', 'room_number', 'square'];
    }

    public function extraFields()
    {
        return ['apartments', 'room', 'buildingInfo'];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartments()
    {
        return $this->hasMany(EsbnApartments::class, ['building' => 'building', 'room_number' => 'room_number', 'square' => 'square']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComplex()
    {
        return $this->hasOne(Complexes::class, ['esbn_id' => 'district'])->via('buildingInfo');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuildingInfo()
    {
        return $this->hasOne(EsbnBuildings::class, ['id' => 'building']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoom()
    {
        return $this->hasOne(EsbnRoomNumbers::class, ['id' => 'room_number']);
    }
}
