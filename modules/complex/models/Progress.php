<?php

namespace app\modules\complex\models;

use app\components\files\Files;
use app\helpers\BehaviorsHelper;

/**
 * This is the model class for table "progress".
 *
 * @property int $id
 * @property int|null $complex_id
 * @property int $year
 * @property int $month
 * @property string|null $description
 * @property int $created
 * @property int $updated
 *
 * @property Complexes $complex
 * @property ProgressImages[] $progressImages
 * @property Files[] $images
 */
class Progress extends \yii\db\ActiveRecord
{

    public $date;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'progress';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date', 'complex_id'], 'required'],
            [['complex_id'], 'integer'],
            [['date'], 'date', 'format' => 'php:Y-m'],
            [['description'], 'string', 'max' => 600],
            [['complex_id'], 'exist', 'skipOnError' => true, 'targetClass' => Complexes::class, 'targetAttribute' => ['complex_id' => 'id']],
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->date) {
            list($year, $month) = explode('-', $this->date);
            $this->year = $year;
            $this->month = $month;
        }
        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'complex_id' => 'Комплекс',
            'date' => 'Месяц и Год',
            'description' => 'Описание',
        ];
    }

    public function behaviors()
    {
        return [BehaviorsHelper::time()];
    }

    public function extraFields()
    {
        return ['images'];
    }


    /**
     * Gets query for [[Images]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(Files::class, ['id' => 'image_id'])->viaTable(ProgressImages::tableName(), ['progress_id' => 'id']);
    }
}
