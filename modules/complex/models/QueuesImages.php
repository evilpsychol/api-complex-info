<?php

namespace app\modules\complex\models;

use app\components\files\Files;
use Yii;

/**
 * This is the model class for table "queues_images".
 *
 * @property int $queue_id
 * @property int|null $image_id
 *
 * @property Files $image
 */
class QueuesImages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'queues_images';
    }

    public static function addImage(int $queueId, Files $file)
    {
        Yii::$app->db->createCommand()->upsert(self::tableName(), [
            'queue_id' => $queueId,
            'image_id' => $file->id,
        ], ['image_id' => $file->id])->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['queue_id'], 'required'],
            [['queue_id', 'image_id'], 'integer'],
            [['queue_id'], 'unique'],
            [['image_id'], 'exist', 'skipOnError' => true, 'targetClass' => Files::class, 'targetAttribute' => ['image_id' => 'id']],
        ];
    }


    /**
     * Gets query for [[Image]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(Files::class, ['id' => 'image_id']);
    }
}
