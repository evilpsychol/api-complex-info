<?php

namespace app\modules\complex\models;

use app\helpers\BehaviorsHelper;
use app\modules\esbn\models\EsbnDistricts;
use app\modules\esbn\models\EsbnDistrictsImages;

/**
 * This is the model class for table "complexes".
 *
 * @property int $id
 * @property string $title
 * @property int|null $esbn_id
 * @property int $created
 *
 * @property EsbnDistricts $esbn
 */
class Complexes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'complexes';
    }

    public function beforeSave($insert)
    {
        if ($insert && !$this->title) {
            $this->title = EsbnDistricts::findOne($this->esbn_id)->title;
        }
        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['esbn_id'], 'required'],
            [['esbn_id'], 'integer'],
            [['esbn_id'], 'unique', 'message' => 'Комплекс уже добавлен'],
            [['title'], 'string', 'max' => 150],
            [['esbn_id'], 'exist', 'skipOnError' => TRUE, 'targetClass' => EsbnDistricts::class, 'targetAttribute' => ['esbn_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'esbn_id' => 'Комплекс из ЕСБН'
        ];
    }

    public function behaviors()
    {
        return [BehaviorsHelper::time('created', FALSE)];
    }

    public function fields()
    {
        return [
            'id',
            'title',
            'created',
        ];
    }

    public function extraFields()
    {
        return ['esbn', 'images'];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEsbn()
    {
        return $this->hasOne(EsbnDistricts::class, ['id' => 'esbn_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(EsbnDistrictsImages::class, ['esbn_district_id' => 'esbn_id']);
    }
}
