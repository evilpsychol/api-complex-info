<?php

namespace app\modules\complex\models;

/**
 * This is the model class for table "buildings_position".
 *
 * @property int $building_id
 * @property float $x
 * @property float $y
 */
class BuildingsPosition extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'buildings_position';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['x', 'y'], 'required'],
            [['x', 'y'], 'number', 'min' => 0, 'max' => 100],
        ];
    }

    public function fields()
    {
        return ['x', 'y'];
    }

}
