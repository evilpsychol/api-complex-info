<?php

namespace app\modules\complex\models;

use app\components\files\Files;
use Yii;

/**
 * This is the model class for table "progress_images".
 *
 * @property int $progress_id
 * @property int $image_id
 *
 * @property Files $image
 * @property Progress $progress
 */
class ProgressImages extends \yii\db\ActiveRecord
{
    public static function addImages(int $progressId, $fileIds)
    {
        foreach ($fileIds as $fileId) {
            Yii::$app->db->createCommand()->insert(self::tableName(), [
                'progress_id' => $progressId,
                'image_id' => $fileId
            ])->execute();
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'progress_images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['progress_id', 'image_id'], 'required'],
            [['progress_id', 'image_id'], 'integer'],
            [['progress_id', 'image_id'], 'unique', 'targetAttribute' => ['progress_id', 'image_id']],
            [['image_id'], 'exist', 'skipOnError' => true, 'targetClass' => Files::class, 'targetAttribute' => ['image_id' => 'id']],
            [['progress_id'], 'exist', 'skipOnError' => true, 'targetClass' => Progress::class, 'targetAttribute' => ['progress_id' => 'id']],
        ];
    }


    /**
     * Gets query for [[Image]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(Files::class, ['id' => 'image_id']);
    }

    /**
     * Gets query for [[Progress]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProgress()
    {
        return $this->hasOne(Progress::class, ['id' => 'progress_id']);
    }
}
