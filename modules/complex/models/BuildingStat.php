<?php

namespace app\modules\complex\models;

use app\modules\esbn\models\EsbnRoomNumbers;

/**
 * This is the model class for table "building_stat".
 *
 * @property int|null $building_id
 * @property int|null $room_number
 * @property int $count_apartments
 * @property int|null $min_price
 * @property int|null $max_price
 * @property string|null $facing
 */
class BuildingStat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'building_stat';
    }

    public function fields()
    {
        $fields = parent::fields();
        $fields['price_str'] = function () {
            $round = str_replace('.', ',', round($this->min_price / 1000000, 1));
            return "от {$round} млн.р.";
        };
        return $fields;
    }

    public function extraFields()
    {
        return ['room'];
    }

    public function getRoom()
    {
        return $this->hasOne(EsbnRoomNumbers::class, ['id' => 'room_number']);
    }

}
