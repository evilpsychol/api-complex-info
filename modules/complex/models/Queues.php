<?php

namespace app\modules\complex\models;

use app\components\files\Files;
use app\helpers\BehaviorsHelper;
use app\modules\esbn\models\EsbnBuildings;

/**
 * This is the model class for table "queues".
 *
 * @property int $id
 * @property int $complex_id
 * @property string $name
 * @property int|null $created
 *
 * @property Complexes $complex
 * @property EsbnBuildings[] $buildings
 * @property Files $image
 */
class Queues extends \yii\db\ActiveRecord
{

    const SCENARIO_UPDATE = 'update';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'queues';
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => ['complex_id', 'name'],
            self::SCENARIO_UPDATE => ['name'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['complex_id', 'name'], 'required'],
            [['complex_id'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['complex_id'], 'exist', 'skipOnError' => true, 'targetClass' => Complexes::class, 'targetAttribute' => ['complex_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'complex_id' => 'Комплекс',
            'name' => 'Название',
        ];
    }

    public function behaviors()
    {
        return [BehaviorsHelper::time('created', FALSE)];
    }

    public function afterDelete()
    {
        if ($this->image) {
            $this->image->delete();
        }
        parent::afterDelete();
    }

    public function fields()
    {
        return [
            'id',
            'name',
            'showName' => function () {
                return $this->name == 'Нет очередей' ? null : $this->name;
            },
            'created'
        ];
    }

    public function extraFields()
    {
        return ['buildings', 'image'];
    }

    /**
     * Gets query for [[Complex]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComplex()
    {
        return $this->hasOne(Complexes::class, ['id' => 'complex_id']);
    }

    /**
     * Gets query for [[QueuesBuildings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getQueuesBuildings()
    {
        return $this->hasMany(QueuesBuilding::class, ['queue_id' => 'id']);
    }

    public function getImage()
    {
        return $this->hasOne(Files::class, ['id' => 'image_id'])->viaTable(QueuesImages::tableName(), ['queue_id' => 'id']);
    }

    public function getBuildings()
    {
        return $this->hasMany(EsbnBuildings::class, ['id' => 'building_id'])->viaTable(QueuesBuilding::tableName(), ['queue_id' => 'id']);
    }
}
