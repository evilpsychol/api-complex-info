<?php

namespace app\modules\complex\models;

/**
 * This is the model class for table "queues_building".
 *
 * @property int $queue_id
 * @property int $building_id
 *
 * @property Queues $queue
 */
class QueuesBuilding extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'queues_building';
    }

    public static function insertBuildings($queueId, $buildingIds)
    {
        foreach ($buildingIds as $buildingId) {
            (new self(['queue_id' => $queueId, 'building_id' => $buildingId]))->save(false);
        }
    }

    /**
     * Gets query for [[Queue]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getQueue()
    {
        return $this->hasOne(Queues::class, ['id' => 'queue_id']);
    }
}
