<?php

namespace app\modules\complex\models\search;

use app\modules\complex\models\ApartmentsGroup;
use app\modules\esbn\models\EsbnApartments;
use Yii;
use yii\data\ActiveDataProvider;


class ApartmentsSearch extends EsbnApartments
{
    public $complex_id;
    public $queue_id;
    public $building_id;
    public $room_type;
    public $ending;
    public $window;
    public $balcony;
    public $status;
    public $facing;
    public $price_min;
    public $price_max;
    public $floor_min;
    public $floor_max;
    public $square_min;
    public $square_max;
    public $kitchen_min;
    public $kitchen_max;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['complex_id'], 'required'],
            [['complex_id', 'queue_id', 'building_id',
                'room_type', 'window', 'facing',
                'price_min', 'price_max',
                'floor_min', 'floor_max',
                'square_min', 'square_max',
                'kitchen_min', 'kitchen_max',
            ], 'integer'],
            [['ending'], 'date', 'format' => 'php:Y-m'],
            [['balcony', 'status'], 'boolean'],
        ];
    }

    /**
     * @param $params
     * @return $this|ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApartmentsGroup::find()
            ->with(explode(',', Yii::$app->request->get('expand')))
            ->joinWith(['apartments.buildingInfo.queue', 'complex'])
            ->groupBy(['apartments_group.building', 'apartments_group.room_number', 'apartments_group.square']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'price' => [
                        'asc' => ['apartments_group.min_price' => SORT_ASC],
                        'desc' => ['apartments_group.min_price' => SORT_DESC],
                    ],
                    'floor' => [
                        'asc' => ['esbn_apartments.floor' => SORT_ASC],
                        'desc' => ['esbn_apartments.floor' => SORT_DESC],
                    ],
                    'ending' => [
                        'asc' => ['esbn_buildings.ending_year' => SORT_ASC, 'esbn_buildings.ending_quarter' => SORT_ASC],
                        'desc' => ['esbn_buildings.ending_year' => SORT_DESC, 'esbn_buildings.ending_quarter' => SORT_DESC],
                    ],
                ]
            ],
        ]);

        $this->load($params, '');

        if (!$this->validate()) {
            return $this;
        }

        $query->andFilterWhere([
            'complexes.id' => $this->complex_id,
            'queues.id' => $this->queue_id,
            'esbn_apartments.building' => $this->building_id,
            'esbn_apartments.facing' => $this->facing,
            'esbn_apartments.status' => $this->status ? 3 : null,
            'esbn_apartments.balcony' => $this->balcony ? 'есть' : null,
            'esbn_apartments.room_number' => $this->room_type,
            'esbn_apartments.cardinal_directions' => $this->window,
        ]);

        $query->andFilterWhere(['>=', 'esbn_apartments.apartment_price', $this->price_min]);
        $query->andFilterWhere(['<=', 'esbn_apartments.apartment_price', $this->price_max]);

        $query->andFilterWhere(['>=', 'esbn_apartments.floor', $this->floor_min]);
        $query->andFilterWhere(['<=', 'esbn_apartments.floor', $this->floor_max]);

        $query->andFilterWhere(['>=', 'esbn_apartments.square', $this->square_min]);
        $query->andFilterWhere(['<=', 'esbn_apartments.square', $this->square_max]);

        $query->andFilterWhere(['>=', 'esbn_apartments.kitchen_square', $this->kitchen_min]);
        $query->andFilterWhere(['<=', 'esbn_apartments.kitchen_square', $this->kitchen_max]);

        if ($this->ending) {
            list($year, $quarter) = explode('-', $this->ending);
            $query->andFilterWhere([
                'esbn_buildings.ending_year' => $year,
                'esbn_buildings.ending_quarter' => $quarter,
            ]);
        }

        return $dataProvider;
    }
}
