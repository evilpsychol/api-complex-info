<?php

namespace app\modules\complex\models;

use app\modules\esbn\models\EsbnBuildings;

/**
 * This is the model class for table "buildings".
 *
 * @property int $esbn_id
 * @property string $name
 * @property string $comment
 */
class Buildings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'buildings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['esbn_id'], 'required'],
            [['name'], 'string', 'max' => 50],
            [['comment'], 'string'],
            [['esbn_id'], 'unique'],
            [['esbn_id'], 'exist', 'skipOnError' => TRUE, 'targetClass' => EsbnBuildings::class, 'targetAttribute' => ['esbn_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'esbn_id' => 'Корпус из ЕСБН',
            'name' => 'Название',
            'comment' => 'Комментарий',
        ];
    }
}
