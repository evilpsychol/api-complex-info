<?php

namespace app\modules\esbn\controllers;

use app\components\base\actions\AllAction;
use app\components\base\BaseApiController;
use app\modules\complex\models\Complexes;
use app\modules\esbn\models\EsbnDistricts;
use app\modules\user\models\Users;
use yii\rest\CreateAction;

/**
 * Default controller for the `complex` module
 */
class ComplexController extends BaseApiController
{
    protected $rules = [
        [
            'allow' => TRUE,
            'roles' => [Users::ROLE_ADMIN],
        ],
    ];

    public function methods()
    {
        return [
            'list' => [
                'class' => AllAction::class,
                'modelClass' => EsbnDistricts::class,
                'orderBy' => 'title'
            ],
        ];
    }

}
