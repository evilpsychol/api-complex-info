<?php

namespace app\modules\esbn\models;

use Yii;

/**
 * This is the model class for table "esbn_readinesses".
 *
 * @property int $id
 * @property string|null $title
 */
class EsbnReadinesses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'esbn_readinesses';
    }


}
