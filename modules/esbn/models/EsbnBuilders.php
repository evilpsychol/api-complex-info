<?php

namespace app\modules\esbn\models;

use Yii;

/**
 * This is the model class for table "esbn_builders".
 *
 * @property int $id
 * @property string|null $title
 */
class EsbnBuilders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'esbn_builders';
    }


}
