<?php

namespace app\modules\esbn\models;

use Yii;

/**
 * This is the model class for table "esbn_building_payment_types".
 *
 * @property int $building_id
 * @property int $payment_type_id
 */
class EsbnBuildingPaymentTypes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'esbn_building_payment_types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['building_id', 'payment_type_id'], 'required'],
            [['building_id', 'payment_type_id'], 'integer'],
            [['building_id', 'payment_type_id'], 'unique', 'targetAttribute' => ['building_id', 'payment_type_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'building_id' => 'Building ID',
            'payment_type_id' => 'Payment Type ID',
        ];
    }
}
