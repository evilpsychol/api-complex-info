<?php

namespace app\modules\esbn\models;

use Yii;

/**
 * This is the model class for table "esbn_metros".
 *
 * @property int $id
 * @property string|null $title
 * @property int|null $line
 */
class EsbnMetros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'esbn_metros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['line'], 'integer'],
            [['title'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'line' => 'Line',
        ];
    }
}
