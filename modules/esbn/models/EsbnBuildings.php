<?php

namespace app\modules\esbn\models;

use app\modules\complex\models\Buildings;
use app\modules\complex\models\BuildingsPosition;
use app\modules\complex\models\BuildingStat;
use app\modules\complex\models\Queues;
use app\modules\complex\models\QueuesBuilding;

/**
 * This is the model class for table "esbn_buildings".
 *
 * @property int $id
 * @property int|null $builder
 * @property int|null $district
 * @property string|null $corp
 * @property int|null $ending_year
 * @property int|null $ending_quarter
 * @property int|null $floors
 * @property int|null $sections
 * @property int|null $street
 * @property int|null $readiness
 * @property int|null $building_type
 * @property int|null $realty_class
 * @property string|null $latitude
 * @property string|null $longitude
 * @property int|null $is_apartments
 * @property string|null $outline
 * @property string|null $name
 *
 * @property EsbnDistricts $districts
 */
class EsbnBuildings extends \yii\db\ActiveRecord
{

    const QUARTERS = [
        1 => 'I',
        2 => 'II',
        3 => 'III',
        4 => 'IV',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'esbn_buildings';
    }

    public function fields()
    {
        $fields = parent::fields();
        $fields['name'] = function () {
            return $this->name;
        };
        $fields['ending'] = function () {
            return $this->ending;
        };
        return $fields;
    }

    public function extraFields()
    {
        return ['params', 'position', 'stat', 'queue'];
    }

    public function getName()
    {
        if ($this->params && $this->params->name) {
            return $this->params->name;
        }
        return $this->corp;
    }

    public function getEnding()
    {
        if (!$this->ending_quarter) {
            return null;
        }
        $quarter = self::QUARTERS[$this->ending_quarter];
        return "$quarter кв. {$this->ending_year}";
    }

    public function getParams()
    {
        return $this->hasOne(Buildings::class, ['esbn_id' => 'id']);
    }

    public function getQueue()
    {
        return $this->hasOne(Queues::class, ['id' => 'queue_id'])->viaTable(QueuesBuilding::tableName(), ['building_id' => 'id']);
    }

    public function getPosition()
    {
        return $this->hasOne(BuildingsPosition::class, ['building_id' => 'id']);
    }

    public function getStat()
    {
        return $this->hasMany(BuildingStat::class, ['building_id' => 'id']);
    }

    public function getDistrict()
    {
        return $this->hasOne(EsbnDistricts::class, ['id' => 'district']);
    }
}
