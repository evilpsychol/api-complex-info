<?php

namespace app\modules\esbn\models;

/**
 * This is the model class for table "esbn_cardinal_directions".
 *
 * @property int $id
 * @property string|null $title
 */
class EsbnCardinalDirections extends \yii\db\ActiveRecord
{
    const NAMES = [
        "ЮЮЗ" => 'Юг и Юго-Запад',
        "ЮЮВ" => 'Юг и Юго-Восток',
        "ЮЗЗ" => 'Юго-Запад и Запад',
        "ЮЗ" => 'Юго-Запад',
        "ЮВВ" => 'Юго-Восток и Восток',
        "ЮВ" => 'Юго-Восток',
        "Ю" => 'Юг',
        "ССЗ" => 'Север и Северо-Запад',
        "ССВ" => 'Север и Северо-Восток',
        "СЗ" => 'Северо-Запад',
        "СВВ" => 'Северо-Восток и Восток',
        "СВ" => 'Северо-Восток',
        "С" => 'Северо',
        "З" => 'Запад',
        "В" => 'Восток',
        "CЗЗ" => 'Северо-Запад и Запад',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'esbn_cardinal_directions';
    }

    public function fields()
    {
        $fields = parent::fields();
        $fields['full'] = function () {
            return self::NAMES[$this->title];
        };
        return $fields;
    }


    public function getApartments()
    {
        return $this->hasMany(EsbnApartments::class, ['cardinal_directions' => 'id']);
    }

}
