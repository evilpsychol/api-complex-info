<?php

namespace app\modules\esbn\models;

use Yii;

/**
 * This is the model class for table "esbn_building_types".
 *
 * @property int $id
 * @property string|null $title
 */
class EsbnBuildingTypes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'esbn_building_types';
    }


}
