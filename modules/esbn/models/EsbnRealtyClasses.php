<?php

namespace app\modules\esbn\models;

use Yii;

/**
 * This is the model class for table "esbn_realty_classes".
 *
 * @property int $id
 * @property string|null $title
 */
class EsbnRealtyClasses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'esbn_realty_classes';
    }


}
