<?php

namespace app\modules\esbn\models;

/**
 *
 * @property int $id
 * @property string|null $title
 */
class EsbnApartmentStatuses extends \yii\db\ActiveRecord
{
    const STATUS_BLOCK = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'esbn_apartment_statuses';
    }


}
