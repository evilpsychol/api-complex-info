<?php

namespace app\modules\esbn\models;

use Yii;

/**
 * This is the model class for table "esbn_near_metros".
 *
 * @property int $metro_id
 * @property int $district_id
 * @property int|null $time_on_foot
 * @property int|null $time_on_car
 */
class EsbnNearMetros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'esbn_near_metros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['metro_id', 'district_id'], 'required'],
            [['metro_id', 'district_id', 'time_on_foot', 'time_on_car'], 'integer'],
            [['metro_id', 'district_id'], 'unique', 'targetAttribute' => ['metro_id', 'district_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'metro_id' => 'Metro ID',
            'district_id' => 'District ID',
            'time_on_foot' => 'Time On Foot',
            'time_on_car' => 'Time On Car',
        ];
    }
}
