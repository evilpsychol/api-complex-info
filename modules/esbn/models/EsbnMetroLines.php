<?php

namespace app\modules\esbn\models;

use Yii;

/**
 * This is the model class for table "esbn_metro_lines".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $color
 */
class EsbnMetroLines extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'esbn_metro_lines';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 100],
            [['color'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'color' => 'Color',
        ];
    }
}
