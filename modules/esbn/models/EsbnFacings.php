<?php

namespace app\modules\esbn\models;

/**
 * This is the model class for table "esbn_facings".
 *
 * @property int $id
 * @property string|null $title
 */
class EsbnFacings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'esbn_facings';
    }

    public function getApartments()
    {
        return $this->hasMany(EsbnApartments::class, ['facing' => 'id']);
    }

}
