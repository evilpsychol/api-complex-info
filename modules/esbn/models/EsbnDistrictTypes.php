<?php

namespace app\modules\esbn\models;

use Yii;

/**
 * This is the model class for table "esbn_district_types".
 *
 * @property int $id
 * @property string|null $title
 */
class EsbnDistrictTypes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'esbn_district_types';
    }


}
