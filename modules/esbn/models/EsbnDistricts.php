<?php

namespace app\modules\esbn\models;

/**
 * This is the model class for table "esbn_districts".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $town
 * @property int|null $townarea
 * @property string|null $latitude
 * @property string|null $longitude
 * @property int|null $location
 * @property int|null $realty_class
 * @property int|null $district_type
 * @property int|null $yandex_building_id
 *
 * @property EsbnBuildings[] $buildings
 */
class EsbnDistricts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'esbn_districts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['townarea', 'location', 'realty_class', 'district_type', 'yandex_building_id'], 'integer'],
            [['title', 'town'], 'string', 'max' => 100],
            [['latitude', 'longitude'], 'string', 'max' => 30],
        ];
    }

    public function extraFields()
    {
        return ['buildings'];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuildings()
    {
        return $this->hasMany(EsbnBuildings::class, ['district' => 'id']);
    }
}
