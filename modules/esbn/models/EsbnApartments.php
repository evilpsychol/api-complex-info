<?php

namespace app\modules\esbn\models;

use app\components\EsbnComponent;

/**
 * This is the model class for table "esbn_apartments".
 *
 * @property int $id
 * @property int|null $building
 * @property int|null $room_number
 * @property int|null $facing
 * @property int|null $apartment_type
 * @property int|null $floor
 * @property float|null $square
 * @property float|null $living_square
 * @property float|null $kitchen_square
 * @property float|null $ceiling_height
 * @property string|null $water_closet
 * @property string|null $balcony
 * @property string|null $terrace
 * @property int|null $apartment_price
 * @property int|null $meter_price
 * @property int|null $base_price
 * @property string|null $cardinal_direction_point_latitude
 * @property string|null $cardinal_direction_point_longitude
 * @property int|null $cardinal_directions
 * @property int|null $status
 * @property string|null $agency_id
 * @property string|null $plane_pic_url
 * @property int|null $floors_in_section
 */
class EsbnApartments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'esbn_apartments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['building', 'room_number', 'facing', 'apartment_type', 'floor', 'apartment_price', 'meter_price', 'base_price', 'cardinal_directions', 'floors_in_section'], 'integer'],
            [['square', 'living_square', 'kitchen_square', 'ceiling_height'], 'number'],
            [['water_closet', 'balcony', 'terrace', 'cardinal_direction_point_latitude', 'cardinal_direction_point_longitude'], 'string', 'max' => 30],
            [['agency_id'], 'string', 'max' => 10],
            [['plane_pic_url'], 'string', 'max' => 250],
        ];
    }

    public function fields()
    {
        $fields = parent::fields();
        $fields['image'] = function () {
            return EsbnComponent::DOMEN . $this->plane_pic_url;
        };
        $fields['block'] = function () {
            return $this->status === EsbnApartmentStatuses::STATUS_BLOCK;
        };
        return $fields;
    }

    public function extraFields()
    {
        return ['buildingInfo', 'facingInfo', 'direction'];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuildingInfo()
    {
        return $this->hasOne(EsbnBuildings::class, ['id' => 'building']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacingInfo()
    {
        return $this->hasOne(EsbnFacings::class, ['id' => 'facing']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDirection()
    {
        return $this->hasOne(EsbnCardinalDirections::class, ['id' => 'cardinal_directions']);
    }

}
