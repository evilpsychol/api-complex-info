<?php

namespace app\modules\esbn\models;

use app\components\EsbnComponent;

/**
 * This is the model class for table "esbn_districts_images".
 *
 * @property int $esbn_district_id
 * @property string $image
 */
class EsbnDistrictsImages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'esbn_districts_images';
    }

    public function fields()
    {
        return [
            'image' => function () {
                return EsbnComponent::DOMEN . $this->image;
            }
        ];
    }
}
