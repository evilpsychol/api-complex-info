<?php

namespace app\modules\esbn\models;

/**
 * This is the model class for table "esbn_room_numbers".
 *
 * @property int $id
 * @property string|null $title
 */
class EsbnRoomNumbers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'esbn_room_numbers';
    }

    public function fields()
    {
        return [
            'id',
            'title',
            'full' => function () {
                return str_replace('ккв', '- комнатные', $this->title);
            }
        ];
    }

    public function getApartments()
    {
        return $this->hasMany(EsbnApartments::class, ['room_number' => 'id']);
    }

}
