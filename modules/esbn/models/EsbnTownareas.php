<?php

namespace app\modules\esbn\models;

use Yii;

/**
 * This is the model class for table "esbn_townareas".
 *
 * @property int $id
 * @property string|null $title
 */
class EsbnTownareas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'esbn_townareas';
    }


}
