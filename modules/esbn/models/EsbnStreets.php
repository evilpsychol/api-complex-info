<?php

namespace app\modules\esbn\models;

use Yii;

/**
 * This is the model class for table "esbn_streets".
 *
 * @property int $id
 * @property string|null $title
 */
class EsbnStreets extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'esbn_streets';
    }


}
