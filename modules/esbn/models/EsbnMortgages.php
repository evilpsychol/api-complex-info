<?php

namespace app\modules\esbn\models;

use Yii;

/**
 * This is the model class for table "esbn_mortgages".
 *
 * @property int $building_id
 * @property int $bank_id
 */
class EsbnMortgages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'esbn_mortgages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['building_id', 'bank_id'], 'required'],
            [['building_id', 'bank_id'], 'integer'],
            [['building_id', 'bank_id'], 'unique', 'targetAttribute' => ['building_id', 'bank_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'building_id' => 'Building ID',
            'bank_id' => 'Bank ID',
        ];
    }
}
