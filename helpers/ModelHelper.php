<?php

namespace app\helpers;

use yii\web\NotFoundHttpException;

class ModelHelper
{
    /**
     * @param $class
     * @param $params
     * @param string $message
     * @return mixed
     * @throws NotFoundHttpException
     */
    public static function find($class, $params, $message = 'Объект не найден')
    {
        $find = $class::findOne($params);
        if (!$find) {
            throw new NotFoundHttpException($message);
        }
        return $find;
    }
}
