<?php

namespace app\helpers;

use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

class BehaviorsHelper
{
    public static function time($created = 'created', $updated = 'updated')
    {
        return [
            'class' => TimestampBehavior::class,
            'updatedAtAttribute' => $updated,
            'createdAtAttribute' => $created,
        ];
    }

    public static function author($attr = 'user_id')
    {
        return [
            'class' => BlameableBehavior::class,
            'createdByAttribute' => $attr,
            'updatedByAttribute' => FALSE,
        ];
    }
}
