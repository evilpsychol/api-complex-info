<?php

namespace app\bootstrap;

use yii\base\Application;
use yii\base\BootstrapInterface;


class Request implements BootstrapInterface
{
    /**
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        if ($app->getRequest()->getMethod() === 'OPTIONS') {
            $app->catchAll = ['site/options'];
        }
    }
}
