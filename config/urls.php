<?php
return [
    /**
     * ПОЛЬЗОВАТЕЛИ
     */
    'POST users/login' => 'user/default/login',
    'GET users/self' => 'user/default/self',
    'POST users/logout' => 'user/default/logout',

    /**
     * КОМПЛЕКСЫ
     */
    'GET complex/list' => 'complex/default/list',
    'POST complex/add' => 'complex/default/add',
    'GET complex/<id:\d+>' => 'complex/default/one',

    'GET complex/flats' => 'complex/flats/list',
    'GET complex/<id:\d+>/flats/filter' => 'complex/flats/filter',

    'POST complex/queues/add' => 'complex/queues/add',
    'POST complex/queues/<id:\d+>' => 'complex/queues/update',
    'POST complex/queues/<id:\d+>/image' => 'complex/queues/image',
    'DELETE complex/queues/<id:\d+>/image' => 'complex/queues/delete-image',
    'DELETE complex/queues/<id:\d+>' => 'complex/queues/delete',
    'GET complex/<id:\d+>/queues/list' => 'complex/queues/list',
    'POST complex/queues/change-building' => 'complex/queues/change-building',

    'POST complex/buildings/<id:\d+>' => 'complex/buildings/change',
    'POST complex/buildings/<id:\d+>/position' => 'complex/buildings/position',

    'POST complex/progress/change' => 'complex/progress/change',
    'GET complex/<id:\d+>/progress' => 'complex/progress/list',
    'DELETE complex/progress/<id:\d+>' => 'complex/progress/delete',
    'POST complex/progress/<id:\d+>/image' => 'complex/progress/image',
    'DELETE complex/progress/image/<id:\d+>' => 'complex/progress/delete-image',

    /**
     * ESBN
     */
    'GET esbn/complex/list' => 'esbn/complex/list',
];
