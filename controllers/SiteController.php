<?php

namespace app\controllers;

use app\components\base\BaseApiController;
use yii\helpers\ArrayHelper;
use yii\rest\OptionsAction;

class SiteController extends BaseApiController
{

    protected $rules = [];

    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'options' => OptionsAction::class,
        ]);
    }
}
