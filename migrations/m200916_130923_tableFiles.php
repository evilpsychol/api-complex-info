<?php

/**
 * Handles the creation of table `files`.
 */
class m200916_130923_tableFiles extends \app\components\base\BaseMigration
{
    public $table = 'files';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'id' => $this->primaryKey(),
            'file_name' => $this->string(50)->notNull(),
            'name' => $this->string(50),
            'path' => $this->string(50)->notNull(),
            'content_type' => $this->string(20)->notNull(),
            'created' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
