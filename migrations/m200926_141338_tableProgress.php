<?php

/**
 * Handles the creation of table `progress`.
 */
class m200926_141338_tableProgress extends \app\components\base\BaseMigration
{
    public $table = 'progress';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'id' => $this->primaryKey(),
            'complex_id' => $this->integer(),
            'year' => $this->integer(4)->notNull(),
            'month' => $this->integer(2)->notNull(),
            'description' => $this->string(600),
            'created' => $this->integer()->notNull(),
            'updated' => $this->integer()->notNull(),
        ]);
        $this->createIndexKey(['complex_id', 'year', 'month']);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
