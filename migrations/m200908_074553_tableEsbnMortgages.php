<?php

/**
 * Handles the creation of table `esbn_mortgages`.
 */
class m200908_074553_tableEsbnMortgages extends \app\components\base\BaseMigration
{
    public $table = 'esbn_mortgages';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'building_id' => $this->integer(),
            'bank_id' => $this->integer(),
        ]);
        $this->addPrimaryKey('pk_' . $this->table, $this->table, ['building_id', 'bank_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
