<?php

/**
 * FK of table `users_tokens`.
 */
class m200909_061857_FKUsersTokens extends \app\components\base\BaseMigration
{
    public $table = 'users_tokens';

    public $tablesRefs = [
        [
            'table' => 'users',
            'column' => 'user_id',
            'refColumn' => 'id',
            'delete' => 'CASCADE',
            'update' => 'CASCADE',
        ],
    ];

    public function up()
    {
        $this->upFk();
    }

    public function down()
    {
        $this->downFk();
    }
}
