<?php

/**
 * Handles the creation of table `buildings`.
 */
class m200915_102952_tableBuildings extends \app\components\base\BaseMigration
{
    public $table = 'buildings';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'esbn_id' => $this->integer()->notNull(),
            'name' => $this->string(50),
        ]);

        $this->createIndexKey(['esbn_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
