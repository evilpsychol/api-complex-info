<?php

/**
 * Handles the creation of table `queues`.
 */
class m200910_142903_tableQueues extends \app\components\base\BaseMigration
{
    public $table = 'queues';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'id' => $this->primaryKey(),
            'complex_id' => $this->integer()->notNull(),
            'name' => $this->string(50)->notNull(),
            'created' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
