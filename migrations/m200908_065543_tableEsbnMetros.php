<?php

/**
 * Handles the creation of table `esbn_metros`.
 */
class m200908_065543_tableEsbnMetros extends \app\components\base\BaseMigration
{
    public $table = 'esbn_metros';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'id' => $this->primaryKey(),
            'title' => $this->string(100),
            'line' => $this->smallInteger(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
