<?php

/**
 * Handles the creation of table `esbn_metro_lines`.
 */
class m200908_065535_tableEsbnMetroLines extends \app\components\base\BaseMigration
{
    public $table = 'esbn_metro_lines';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'id' => $this->primaryKey(),
            'title' => $this->string(100),
            'color' => $this->string(50),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
