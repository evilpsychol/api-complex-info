<?php

/**
 * Handles the creation of table `buildings`.
 */
class m201003_105843_changeBuildings extends \app\components\base\BaseMigration
{
    public $table = 'buildings';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->addColumns($this->columns());
    }

    private function columns()
    {
        return [
            'comment' => $this->text(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropColumns($this->columns());
    }
}
