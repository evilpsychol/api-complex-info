<?php

/**
 * Handles the creation of table `esbn_districts_images`.
 */
class m201001_071618_tableEsbnDistrictsImages extends \app\components\base\BaseMigration
{
    public $table = 'esbn_districts_images';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'esbn_district_id' => $this->integer()->notNull(),
            'image' => $this->string(250)->notNull(),
        ]);
        $this->createIndexKey(['image']);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
