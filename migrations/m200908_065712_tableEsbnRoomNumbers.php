<?php

/**
 * Handles the creation of table `esbn_room_numbers`.
 */
class m200908_065712_tableEsbnRoomNumbers extends \app\components\base\BaseMigration
{
    public $table = 'esbn_room_numbers';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'id' => $this->primaryKey(),
            'title' => $this->string(100),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
