<?php

/**
 * Handles the creation of table `buildings_position`.
 */
class m200917_131300_tableBuildingsPosition extends \app\components\base\BaseMigration
{
    public $table = 'buildings_position';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'building_id' => $this->integer()->notNull(),
            'x' => $this->decimal(6, 3)->notNull(),
            'y' => $this->decimal(6, 3)->notNull(),
        ]);
        $this->createIndexKey(['building_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
