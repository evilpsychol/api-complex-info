<?php

/**
 * Handles the creation of table `esbn_near_metros`.
 */
class m200908_074526_tableEsbnNearMetros extends \app\components\base\BaseMigration
{
    public $table = 'esbn_near_metros';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'metro_id' => $this->integer(),
            'district_id' => $this->integer(),
            'time_on_foot' => $this->integer(),
            'time_on_car' => $this->integer(),
        ]);

        $this->addPrimaryKey('pk_' . $this->table, $this->table, ['metro_id', 'district_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
