<?php

/**
 * FK of table `progress`.
 */
class m200926_142010_FKProgress extends \app\components\base\BaseMigration
{
    public $table = 'progress';

    public $tablesRefs = [
        [
            'table' => 'complexes',
            'column' => 'complex_id',
            'refColumn' => 'id',
            'delete' => 'CASCADE',
            'update' => 'CASCADE',
        ],
    ];

    public function up()
    {
        $this->upFk();
    }

    public function down()
    {
        $this->downFk();
    }
}
