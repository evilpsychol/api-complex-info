<?php

/**
 * Handles the creation of table `esbn_locations`.
 */
class m200908_065808_tableEsbnLocations extends \app\components\base\BaseMigration
{
    public $table = 'esbn_locations';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'id' => $this->primaryKey(),
            'title' => $this->string(100),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
