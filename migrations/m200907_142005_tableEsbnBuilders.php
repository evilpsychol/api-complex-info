<?php

/**
 * Handles the creation of table `esbn_builders`.
 */
class m200907_142005_tableEsbnBuilders extends \app\components\base\BaseMigration
{
    public $table = 'esbn_builders';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'id' => $this->primaryKey(),
            'title' => $this->string(100),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
