<?php

/**
 * Handles the creation of table `users_tokens`.
 */
class m200909_061517_tableUsersTokens extends \app\components\base\BaseMigration
{
    public $table = 'users_tokens';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'token' => $this->string(50)->notNull(),
            'ip' => $this->string(50)->notNull(),
            'expires' => $this->integer()->notNull(),
            'created' => $this->integer()->notNull(),
        ]);
        $this->createIndexKey(['token'], FALSE);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
