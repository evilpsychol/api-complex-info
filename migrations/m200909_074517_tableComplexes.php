<?php

/**
 * Handles the creation of table `complexes`.
 */
class m200909_074517_tableComplexes extends \app\components\base\BaseMigration
{
    public $table = 'complexes';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'id' => $this->primaryKey(),
            'title' => $this->string(150)->notNull(),
            'esbn_id' => $this->integer(),
            'created' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
