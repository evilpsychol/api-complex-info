<?php

/**
 * Handles the creation of table `esbn_apartment_types`.
 */
class m200908_065645_tableEsbnApartmentTypes extends \app\components\base\BaseMigration
{
    public $table = 'esbn_apartment_types';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'id' => $this->primaryKey(),
            'title' => $this->string(100),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
