<?php

use yii\db\Migration;

/**
 * Class m201004_113520_viewApartmentsGroup
 */
class m201004_113520_viewApartmentsGroup extends Migration
{
    public $name = 'apartments_group';

    public function up()
    {
        $this->execute("CREATE VIEW `{$this->name}` AS " . file_get_contents(Yii::getAlias("@app/migrations/sql/views/{$this->name}.sql")));
    }

    public function down()
    {
        $this->execute("DROP VIEW {$this->name}");
    }
}
