<?php

/**
 * Handles the creation of table `queues_building`.
 */
class m200910_143115_tableQueuesBuilding extends \app\components\base\BaseMigration
{
    public $table = 'queues_building';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'queue_id' => $this->integer(),
            'building_id' => $this->integer(),
        ]);

        $this->addPrimaryKey('pk_' . $this->table, $this->table, ['queue_id', 'building_id']);
        $this->createIndexKey(['building_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
