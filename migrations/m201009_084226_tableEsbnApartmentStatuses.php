<?php

/**
 * Handles the creation of table `esbn_apartment_statuses`.
 */
class m201009_084226_tableEsbnApartmentStatuses extends \app\components\base\BaseMigration
{
    public $table = 'esbn_apartment_statuses';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'id' => $this->primaryKey(),
            'title' => $this->string(100),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
