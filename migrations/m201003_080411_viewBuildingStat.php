<?php

use yii\db\Migration;

/**
 * Class m201003_080411_viewBuildingStat
 */
class m201003_080411_viewBuildingStat extends Migration
{
    public $name = 'building_stat';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->execute("CREATE VIEW `{$this->name}` AS " . file_get_contents(Yii::getAlias("@app/migrations/sql/views/{$this->name}.sql")));
    }

    public function down()
    {
        $this->execute("DROP VIEW {$this->name}");
    }
}
