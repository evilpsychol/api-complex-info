<?php

/**
 * Handles the creation of table `users`.
 */
class m200908_144634_tableUsers extends \app\components\base\BaseMigration
{
    public $table = 'users';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'id' => $this->primaryKey(),
            'login' => $this->string(50)->notNull(),
            'password_hash' => $this->string(100)->notNull(),
            'role' => $this->smallInteger()->notNull(),
            'active' => $this->boolean()->defaultValue(true),
            'created' => $this->integer()
        ]);

        $this->createIndexKey(['login']);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
