<?php

/**
 * Handles the creation of table `esbn_building_payment_types`.
 */
class m200908_074628_tableEsbnBuildingPaymentTypes extends \app\components\base\BaseMigration
{
    public $table = 'esbn_building_payment_types';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'building_id' => $this->integer(),
            'payment_type_id' => $this->integer(),
        ]);
        $this->addPrimaryKey('pk_' . $this->table, $this->table, ['building_id', 'payment_type_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
