<?php

/**
 * Handles the creation of table `esbn_districts`.
 */
class m200908_065836_tableEsbnDistricts extends \app\components\base\BaseMigration
{
    public $table = 'esbn_districts';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'id' => $this->primaryKey(),
            'title' => $this->string(100),
            'town' => $this->string(100),
            'townarea' => $this->integer(),
            'latitude' => $this->string(30),
            'longitude' =>$this->string(30),
            'location' => $this->integer(),
            'realty_class' => $this->integer(),
            'district_type' => $this->integer(),
            'yandex_building_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
