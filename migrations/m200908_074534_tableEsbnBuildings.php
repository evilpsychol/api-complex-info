<?php

/**
 * Handles the creation of table `esbn_buildings`.
 */
class m200908_074534_tableEsbnBuildings extends \app\components\base\BaseMigration
{
    public $table = 'esbn_buildings';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'id' => $this->primaryKey(),
            'builder' => $this->integer(),
            'district' => $this->integer(),
            'corp' => $this->string(50),
            'ending_year' => $this->integer(4),
            'ending_quarter' => $this->smallInteger(),
            'floors' => $this->integer(3),
            'sections' => $this->integer(3),
            'street' => $this->integer(),
            'readiness' => $this->integer(),
            'building_type' => $this->integer(),
            'realty_class' => $this->integer(),
            'latitude' => $this->string(30),
            'longitude' => $this->string(30),
            'is_apartments' => $this->boolean(),
            'outline' => $this->json(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
