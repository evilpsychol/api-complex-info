<?php

/**
 * Handles the creation of table `esbn_streets`.
 */
class m200908_065521_tableEsbnStreets extends \app\components\base\BaseMigration
{
    public $table = 'esbn_streets';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'id' => $this->primaryKey(),
            'title' => $this->string(100),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
