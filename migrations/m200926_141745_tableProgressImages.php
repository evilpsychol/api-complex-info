<?php

/**
 * Handles the creation of table `progress_images`.
 */
class m200926_141745_tableProgressImages extends \app\components\base\BaseMigration
{
    public $table = 'progress_images';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'progress_id' => $this->integer()->notNull(),
            'image_id' => $this->integer()->notNull(),
        ]);

        $this->createIndexKey(['progress_id', 'image_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
