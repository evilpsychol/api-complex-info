<?php

/**
 * Handles the creation of table `esbn_apartments`.
 */
class m200929_085608_changeEsbnApartments extends \app\components\base\BaseMigration
{
    public $table = 'esbn_apartments';

    private function columns()
    {
        return [
            'section_scheme' => $this->integer(3),
            'riser_scheme' => $this->integer(3),
            'status' => $this->integer(3),
        ];
    }

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->addColumns($this->columns());
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropColumns($this->columns());
    }
}
