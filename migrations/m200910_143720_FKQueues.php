<?php

/**
 * FK of table `queues`.
 */
class m200910_143720_FKQueues extends \app\components\base\BaseMigration
{
    public $table = 'queues';

    public $tablesRefs = [
        [
            'table' => 'complexes',
            'column' => 'complex_id',
            'refColumn' => 'id',
            'delete' => 'CASCADE',
            'update' => 'CASCADE',
        ],
    ];

    public function up()
    {
        $this->upFk();
    }

    public function down()
    {
        $this->downFk();
    }
}
