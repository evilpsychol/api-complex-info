<?php

/**
 * FK of table `queues_building`.
 */
class m200910_143343_FKQueuesBuilding extends \app\components\base\BaseMigration
{
    public $table = 'queues_building';

    public $tablesRefs = [
        [
            'table' => 'queues',
            'column' => 'queue_id',
            'refColumn' => 'id',
            'delete' => 'CASCADE',
            'update' => 'CASCADE',
        ],
    ];

    public function up()
    {
        $this->upFk();
    }

    public function down()
    {
        $this->downFk();
    }
}
