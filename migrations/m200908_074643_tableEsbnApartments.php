<?php

/**
 * Handles the creation of table `esbn_apartments`.
 */
class m200908_074643_tableEsbnApartments extends \app\components\base\BaseMigration
{
    public $table = 'esbn_apartments';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'id' => $this->primaryKey(),
            'building' => $this->integer(),
            'room_number' => $this->integer(),
            'facing' => $this->integer(),
            'apartment_type' => $this->integer(),
            'floor' => $this->integer(),
            'square' => $this->decimal(8,2),
            'living_square' => $this->decimal(8,2),
            'kitchen_square' => $this->decimal(8,2),
            'ceiling_height' => $this->decimal(8,2),
            'water_closet' => $this->string(30),
            'balcony' => $this->string(30),
            'terrace' => $this->string(30),
            'apartment_price' => $this->integer(),
            'meter_price' => $this->integer(),
            'base_price' => $this->integer(),
            'cardinal_direction_point_latitude' => $this->string(30),
            'cardinal_direction_point_longitude' => $this->string(30),
            'cardinal_directions' => $this->integer(),
            'agency_id' => $this->string(10),
            'plane_pic_url' => $this->string(250),
            'floors_in_section' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
