<?php

/**
 * Handles the creation of table `queues_images`.
 */
class m200916_131206_tableQueuesImages extends \app\components\base\BaseMigration
{
    public $table = 'queues_images';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'queue_id' => $this->integer()->notNull(),
            'image_id' => $this->integer(),
        ]);

        $this->createIndexKey(['queue_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
