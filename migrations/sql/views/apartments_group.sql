SELECT ea.building,
       ea.room_number,
       ea.square,
       min(ea.apartment_price) as min_price,
       max(ea.apartment_price) as max_price,
       count(*)                as count
FROM esbn_apartments ea
         LEFT JOIN esbn_buildings eb ON ea.building = eb.id
         RIGHT JOIN complexes c ON eb.district = c.esbn_id
WHERE ea.building is not null
  and ea.room_number is not null
  and ea.square is not null
group by ea.building, ea.room_number, ea.square;
