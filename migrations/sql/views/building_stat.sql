select eb.id              as building_id,
       ea.room_number,
       count(ea.id)       as count_apartments,
       min(ea.apartment_price) as min_price,
       max(ea.apartment_price) as max_price,
       GROUP_CONCAT(DISTINCT ef.title SEPARATOR ', ') as facing
from esbn_buildings eb
         right join complexes c on c.esbn_id = eb.district
         left join esbn_apartments ea on ea.building = eb.id
         left join esbn_room_numbers ern on ea.room_number = ern.id
         left join esbn_facings ef on ef.id=ea.facing
where ea.id is not null
group by eb.id, ea.room_number
