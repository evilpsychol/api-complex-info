<?php

/**
 * FK of table `progress_images`.
 */
class m200926_142037_FKProgressImages extends \app\components\base\BaseMigration
{
    public $table = 'progress_images';

    public $tablesRefs = [
        [
            'table' => 'files',
            'column' => 'image_id',
            'refColumn' => 'id',
            'delete' => 'CASCADE',
            'update' => 'CASCADE',
        ],
        [
            'table' => 'progress',
            'column' => 'progress_id',
            'refColumn' => 'id',
            'delete' => 'CASCADE',
            'update' => 'CASCADE',
        ],
    ];

    public function up()
    {
        $this->upFk();
    }

    public function down()
    {
        $this->downFk();
    }
}
