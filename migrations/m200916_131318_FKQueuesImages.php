<?php

/**
 * FK of table `queues_images`.
 */
class m200916_131318_FKQueuesImages extends \app\components\base\BaseMigration
{
    public $table = 'queues_images';

    public $tablesRefs = [
        [
            'table' => 'files',
            'column' => 'image_id',
            'refColumn' => 'id',
            'delete' => 'SET NULL',
            'update' => 'CASCADE',
        ],
    ];

    public function up()
    {
        $this->upFk();
    }

    public function down()
    {
        $this->downFk();
    }
}
