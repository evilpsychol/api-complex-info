<?php

/**
 * Handles the creation of table `esbn_townareas`.
 */
class m200908_065508_tableEsbnTownareas extends \app\components\base\BaseMigration
{
    public $table = 'esbn_townareas';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'id' => $this->primaryKey(),
            'title' => $this->string(100),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
