<?php

/**
 * Handles the creation of table `esbn_cardinal_directions`.
 */
class m200908_065738_tableEsbnCardinalDirections extends \app\components\base\BaseMigration
{
    public $table = 'esbn_cardinal_directions';

    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->newTable([
            'id' => $this->primaryKey(),
            'title' => $this->string(10),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->removeTable();
    }
}
